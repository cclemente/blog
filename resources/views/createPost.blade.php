<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<style media="screen">
.post___input {
  display: inline-block;

  width: 24%;
  padding: .2em .5em;

  border: 1px solid #eaeaea;
  border-radius: 1.5em;
}
.post-actions__label {
  cursor: pointer;
}

.widget-post__actions {
  width: 100%;
  padding: .5em;
}
.post--actions {
  position: relative;
  padding: .5em;

  background-color: #f5f5f5;
  color: #a2a6a7;
}
.post-actions__attachments {
  display: inline-block;
  width: 60%;
}
.attachments--btn {
  background-color: #eaeaea;
  color: #007582;

  border-radius: 1.5em;
}
.entrada{
  height: 300px;
  width:485px
}
.post-actions__widget {
  display: inline-block;
  width: 38%;
  text-align: right;
}
.post-actions__publish {
  width: 120px;

  background-color: #008391;
  color: #fff;

  border-radius: 1.5em;
}

.scroller::-webkit-scrollbar {
  display: none;
}

.is--hidden {
  display: none;
}

.sr-only {
  width: 1px;
  height: 1px;

  clip: rect(1px, 1px, 1px, 1px);
  -webkit-clip-path: inset(50%);
  clip-path: inset(50%);

  overflow: hidden;
  white-space: nowrap;

  position: absolute;
  top: 0;

}


/* *  Placeholder contrast * */
::-webkit-input-placeholder {
  color: #666;
}
::-moz-placeholder {
  color: #666;
}
:-ms-input-placeholder {
  color: #666;
}
:-moz-placeholder {
  color: #666;
}

</style>
  </head>
  <body
  <nav class="navbar navbar-default navbar-fixed-top">
<div class="container">
<div class="navbar-header">
  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
  </button>
  <a class="navbar-brand" href="#">Blog</a>
</div>
<div id="navbar" class="navbar-collapse collapse">
  <ul class="nav navbar-nav navbar-right">
    <li><a href="#">Home</a></li>
    <li class="active"><a href="{{ url('/createPost') }}">Create Post</a></li>
    <li class="dropdown">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Mi perfil <span class="caret"></span></a>
      <ul class="dropdown-menu" role="menu">
        <li><a href="#">Action</a></li>
        <li><a href="#">Another action</a></li>
        <li><a href="#">Something else here</a></li>
        <li class="divider"></li>
        <li class="dropdown-header">Nav header</li>
        <li><a href="#">Separated link</a></li>
        <li><a href="#">One more separated link</a></li>
      </ul>
    </li>
  </ul>
</div><!--/.nav-collapse -->
</div>
</nav>

    <div class="container center_div" aria-labelledby="post-header-title">
      <div class="widget-post__header">
        <h2 class="widget-post__title" id="post-header-title">
           <i class="fa fa-pencil" aria-hidden="true"></i>
          Write Me
        </h2>
      </div>
      <form id="widget-form" method="POST" action="{{ url('/crearPost') }}" name="form" aria-label="post widget">
        <div class="form-group">
          <label for="exampleInputPassword1">Title</label>
          <input type="text" name="titulo" class="form-control" id="exampleInputPassword1">
        </div>
        <div class="widget-post__content">
          <label for="post-content" class="sr-only">Share</label>
          <textarea class="form-control" id="exampleFormControlTextarea1" rows="5"></textarea>
        </div>
        <div class="widget-post__options is--hidden" id="stock-options">
        </div>
        <div class="widget-post__actions post--actions">
          <div class="post-actions__attachments">
            <input type="file" id="upload-image" name="urlfoto" accept="image/*" multiple>
          </div>
          <div class="post-actions__widget">
            <button class="btn post-actions__publish">Publish</button>
          </div>
        </div>
      </form>
    </div>

        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/js/bootstrap.min.js"></script>

  </body>
</html>
