<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaPost extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
      if (!Schema::hasTable('post')) {
          Schema::create('post', function (Blueprint $table) {
            $table->increments('id_post');
            $table->string('titulo');
            $table->string('cuerpo');
            $table->string('slug');
            $table->integer('id_imagen')->unsigned();
            $table->foreign('id_imagen')->references('id_imagen')->on('imagen');
            $table->integer('id')->unsigned();
            $table->foreign('id')->references('id')->on('users');
          });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
      Schema::dropIfExists('post');
    }
}
